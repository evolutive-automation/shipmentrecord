package com.evolutivelabs.shipmentRecord.service;

import com.evolutivelabs.shipmentRecord.database.entity.Shipment;
import com.evolutivelabs.shipmentRecord.database.entity.ShipmentItem;
import com.evolutivelabs.shipmentRecord.database.repository.ShipmentItemRepository;
import com.evolutivelabs.shipmentRecord.database.repository.ShipmentRepository;
import com.evolutivelabs.shipmentRecord.model.KafkaMessage;
import com.evolutivelabs.shipmentRecord.model.KafkaMessageItem;
import com.evolutivelabs.shipmentRecord.model.KafkaMessagePayLoad;
import com.evolutivelabs.shipmentRecord.model.item.ItemCase;
import com.evolutivelabs.shipmentRecord.utils.DuplicateUtils;
import com.evolutivelabs.shipmentRecord.utils.ShipmentUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.internal.util.collections.CollectionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * shpment相關的解析及儲存邏輯
 */
@Service
public class ShipmentService {
    private static final Logger logger = LoggerFactory.getLogger(ShipmentService.class);

    // sku 拆解pattern
    private static Pattern SKU_PATTERN =
            Pattern.compile("^(([a-z]{2,})([0-9]{2})([0-9]{3})([a-z][0-9]|[0-9]{2})([a-z]?))-?([a-z0-9]{4,}|)",
                    Pattern.CASE_INSENSITIVE);

    private final ShipmentRepository shipmentRepository;
    private final ShipmentItemRepository shipmentItemRepository;
    private final ObjectMapper objectMapper;

    public ShipmentService(ShipmentRepository shipmentRepository, ShipmentItemRepository shipmentItemRepository, ObjectMapper objectMapper) {
        this.shipmentRepository = shipmentRepository;
        this.shipmentItemRepository = shipmentItemRepository;
        this.objectMapper = objectMapper;
    }

    /**
     * 儲存從kafka取出的訊息
     *
     * @param kafkaMessages
     * @param processTime
     */
    @Transactional(rollbackFor = Exception.class)
    public void saveMessage(List<KafkaMessage> kafkaMessages, LocalDateTime processTime) throws Exception {
        List<Shipment> shipments = new ArrayList<>();
        List<ShipmentItem> shipmentItems = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();

        for (KafkaMessage kafkaMessage : kafkaMessages) {
            KafkaMessagePayLoad payLoad = kafkaMessage.getPayload();
            Shipment shipment = new Shipment();

            DuplicateUtils.copyValue(payLoad, shipment, true);

            shipment.setReceiveTime(processTime);

            List<ShipmentItem> items = getItems(payLoad);

            shipment.setType(ShipmentUtils.determinType(items));

            customized(shipment, items.stream().filter(item -> item.getCustomized() == 1).collect(Collectors.toList()));


            shipments.add(shipment);
            shipmentItems.addAll(items);

        }

        shipmentRepository.saveAll(shipments);
        shipmentItemRepository.saveAll(shipmentItems);

        logger.info("message process size: {}, start: {}, end: {}", kafkaMessages.size(), now, LocalDateTime.now());
    }

    /**
     * 其餘客製化相關欄位處理
     *
     * @param shipment
     * @param customizedItems
     */
    private void customized(Shipment shipment, List<ShipmentItem> customizedItems) {
        if (customizedItems.isEmpty()) return;

        BiFunction<Integer, ItemCase, Integer> biFunction = (partialResult, itemcase) ->
                partialResult + itemcase.getQuantity();

        List<ItemCase> ssaList = ShipmentUtils.itemCaseByProduct(customizedItems, "ssa");

        shipment.setSsaAndroidQuantity(ssaList.stream()
                .reduce(0, (partialResult, itemcase) -> partialResult + itemcase.getAndroidQuantity(),
                        Integer::sum));

        shipment.setSsaAppleQuantity(ssaList.stream()
                .reduce(0, (partialResult, itemcase) -> partialResult + itemcase.getAppleQuantity(),
                        Integer::sum));

        shipment.setModQuantity(ShipmentUtils.itemCaseByProduct(customizedItems, "mod").stream()
                .reduce(0, biFunction, Integer::sum));

        shipment.setAirpodQuantity(ShipmentUtils.itemCaseByProduct(customizedItems, "airpod").stream()
                .reduce(0, biFunction, Integer::sum));

        shipment.setCcaQuantity(ShipmentUtils.itemCaseByProduct(customizedItems, "cca").stream()
                .reduce(0, biFunction, Integer::sum));

        // SSA MOD CCA AIRPOD
        StringBuilder binaryString = new StringBuilder();
        binaryString.append(shipment.getSsaAndroidQuantity() > 0 || shipment.getSsaAppleQuantity() > 0 ? "1" : "0");
        binaryString.append(shipment.getModQuantity() > 0 ? "1" : "0");
        binaryString.append(shipment.getCcaQuantity() > 0 ? "1" : "0");
        binaryString.append(shipment.getAirpodQuantity() > 0 ? "1" : "0");

        shipment.setClassification(Integer.valueOf(binaryString.toString(), Character.MIN_RADIX));

    }

    /**
     * 取得payLoad底下所有item
     *
     * @param payLoad
     * @return
     */
    private List<ShipmentItem> getItems(KafkaMessagePayLoad payLoad) throws Exception {
        List<ShipmentItem> items = new ArrayList<>();
        if (CollectionHelper.isNotEmpty(payLoad.getItems())) {
            for (KafkaMessageItem kafkaMessageItem : payLoad.getItems()) {
                ShipmentItem item = new ShipmentItem();
                DuplicateUtils.copyValue(kafkaMessageItem, item, true);
                item.setShipmentId(payLoad.getId());

                item.setDisplayTitle(kafkaMessageItem.getProduct_title());//TODO

                Map<String, String> codes = analysisSku(kafkaMessageItem.getSku());
                item.setCaseSku(codes.get("caseSku"));
                item.setBrandCode(codes.get("brandCode"));
                item.setCaseTypeCode(codes.get("caseType"));
                item.setColorCode(codes.get("colorCode"));
                item.setDeviceCode(codes.get("deviceCode"));
                item.setDesignCode(codes.get("designCode"));

                items.add(item);
            }
        }


        return items;
    }

    /**
     * 分析sku <br>
     * 範例： NPB0114826L-XF19 <br>
     * caseSku = NPB0114826L <br>
     * caseType = NPB <br>
     * brandCode = 01 <br>
     * deviceCode = 148 <br>
     * colorCode = 26 <br>
     * designCode = XF19
     *
     * @param sku 商品代碼
     * @return
     */
    private Map<String, String> analysisSku(String sku) {
        Map<String, String> groups = new HashMap<>();
        Matcher matcher = SKU_PATTERN.matcher(sku);
        if (matcher.matches()) {
            groups.put("caseSku", matcher.group(1));
            groups.put("caseType", matcher.group(2));
            groups.put("brandCode", matcher.group(3));
            groups.put("deviceCode", matcher.group(4));
            groups.put("colorCode", matcher.group(5));
            groups.put("designCode", matcher.group(7));
        } else {
            groups.put("caseSku", sku);
        }
        return groups;
    }

}
