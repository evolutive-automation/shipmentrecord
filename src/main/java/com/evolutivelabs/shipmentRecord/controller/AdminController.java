package com.evolutivelabs.shipmentRecord.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/api/admin")
@RestController
public class AdminController {
    private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
    private final ApplicationContext ctx;

    public AdminController(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    @PostMapping("/shutdown")
    public void shutdown(HttpServletRequest request) {
        logger.info("shutdown with user addr : {}", request.getRemoteAddr());
        ((ConfigurableApplicationContext) ctx).close();
    }
}
