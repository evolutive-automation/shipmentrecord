package com.evolutivelabs.shipmentRecord.utils;

import com.evolutivelabs.shipmentRecord.database.entity.ShipmentItem;
import com.evolutivelabs.shipmentRecord.model.item.BrandsCode;
import com.evolutivelabs.shipmentRecord.model.item.CaseType;
import com.evolutivelabs.shipmentRecord.model.item.ItemCase;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 訂單的欄位判斷工具
 */
public class ShipmentUtils {
    private ShipmentUtils() {}

    /**
     * 判斷符合什麼type
     * @see Type
     * @param items
     * @return
     */
    public static String determinType(List<ShipmentItem> items) {
        String type = null;
        for (Type typeEnum : Type.values()) {
            if (typeEnum.check(items)) {
                type = typeEnum.name();
            }
        }
        return type;
    }

    /**
     * 解析各商品品項數量
     * @param items
     * @param caseType
     * @see CaseType
     * @return
     */
    public static ItemCase itemCase(List<ShipmentItem> items, CaseType caseType) {
        ItemCase itemCase = new ItemCase();
        List<ShipmentItem> caseTypeItems = items.stream()
                .filter(item -> caseType.name().equalsIgnoreCase(item.getCaseTypeCode()))
                .collect(Collectors.toList());

        Integer total = caseTypeItems.stream()
                .reduce(0, (partialQuantityResult, item) -> partialQuantityResult + item.getQuantity(),
                        Integer::sum);

        Integer appleQuantity = caseTypeItems.stream()
                .filter(item -> BrandsCode.APPLE.code().equals(item.getBrandCode()))
                .reduce(0, (partialQuantityResult, item) -> partialQuantityResult + item.getQuantity(),
                        Integer::sum);

        itemCase.setQuantity(total);
        itemCase.setAppleQuantity(appleQuantity);
        itemCase.setAndroidQuantity(total - appleQuantity);
        itemCase.setCaseType(caseType);

        return itemCase;
    }

    /**
     * 解析各商品品項數量
     * @param items
     * @param caseTypes
     * @see CaseType
     * @return
     */
    public static List<ItemCase> listItemCase(List<ShipmentItem> items, CaseType... caseTypes) {
        List<ItemCase> itemCases = new ArrayList<>();
        for (CaseType caseType : caseTypes) {
            itemCases.add(itemCase(items, caseType));
        }

        return itemCases;
    }

    /**
     * 解析各商品品項數量
     * @param items
     * @param product 產品線
     * @see CaseType 裡的 product
     * @return
     */
    public static List<ItemCase> itemCaseByProduct(List<ShipmentItem> items, String product) {
        List<ItemCase> itemCases = new ArrayList<>();
        for (CaseType caseType : CaseType.getCaseTypesByProduct(product)) {
            itemCases.add(itemCase(items, caseType));
        }
        return itemCases;
    }


    private interface Operator {
        Boolean execute(Long customCount, Long normalCount);
    }

    private enum Type {
        S((v1, v2) -> v1 == 1 && v2 == 0),
        A((v1, v2) -> v1 == 1 && v2 > 0),
        B((v1, v2) -> v1 >= 2 && v2 >= 0);

        private Operator operator;

        Type(Operator operator) {
            this.operator = operator;
        }

        public Boolean check(List<ShipmentItem> items) {
            return this.operator.execute(
                    items.stream().filter(item -> item.getCustomized() == 1).count(),
                    items.stream().filter(item -> item.getCustomized() == 0).count());
        }

    }



}
