package com.evolutivelabs.shipmentRecord.utils;

import com.evolutivelabs.shipmentRecord.annotation.Duplicate;
import com.mysql.cj.util.StringUtils;
import org.springframework.dao.DuplicateKeyException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * 對 Duplicate annotation做解析，並複製數值
 * @see Duplicate
 */
public class DuplicateUtils {
    private DuplicateUtils() {}

    /**
     * 針對有Duplicate annotation欄位做數值複製
     * @param source
     * @param target
     * @param isStrict 是否做嚴謹判斷，如果taget有Duplicate而source沒有的話，就丟出錯誤
     * @throws Exception
     */
    public static void copyValue(Object source, Object target, Boolean isStrict) throws Exception {
        Map<String, DuplicateField> sourceValue = getInnerField(source);
        Map<String, DuplicateField> targetValue = getInnerField(target);

        setTargetValue(target, sourceValue, targetValue, isStrict);
    }

    private static Object setTargetValue(Object target, Map<String, DuplicateField> sourceValue, Map<String, DuplicateField> targetValue, Boolean isStrict) throws NoSuchMethodException, NoSuchFieldException, InvocationTargetException, InstantiationException, IllegalAccessException {

        for (DuplicateField targetDuplicateField : targetValue.values()) {
            DuplicateField sourceDuplicateField = sourceValue.get(targetDuplicateField.getDuplicateName());
            if (isStrict && sourceDuplicateField == null)
                throw new RuntimeException(String.format("source field is not found name: %s", targetDuplicateField.getDuplicateName()));

            if (sourceDuplicateField == null)
                continue;

            Field targetField = target.getClass().getDeclaredField(targetDuplicateField.getFieldName());
            Duplicate duplicate = targetField.getAnnotation(Duplicate.class);
            targetField.setAccessible(true);

            if (sourceDuplicateField.getValue() != null) {
                if (duplicate.customized()) {
                    targetField.set(target, setTargetValue(targetField.getType().getDeclaredConstructor().newInstance(),
                            (Map<String, DuplicateField>) sourceDuplicateField.getValue(),
                            (Map<String, DuplicateField>) targetDuplicateField.getValue(), isStrict));
                } else {
                    targetField.set(target, sourceDuplicateField.getValue());
                }
            }
        }

        return target;
    }

    private static Map<String, DuplicateField> getInnerField(Object object) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {
        if (object == null) return null;
        Map<String, DuplicateField> result = new HashMap<>();
        for (Field field : object.getClass().getDeclaredFields()) {
            Duplicate duplicate = field.getAnnotation(Duplicate.class);
            if (duplicate != null) {
                String name = duplicate.value();
                if (StringUtils.isNullOrEmpty(name)) {
                    name = field.getName();
                }
                field.setAccessible(true);

                if (result.containsKey(name)) {
                    throw new DuplicateKeyException(String.format("name: [[%s]] is duplicate!!", name));
                }

                if (duplicate.customized()) {
                    Object obj = field.get(object);
                    if (obj == null) obj = field.getType().getDeclaredConstructor().newInstance();
                    result.put(name, new DuplicateField(name, field.getName(), getInnerField(obj)));
                } else {
                    result.put(name, new DuplicateField(name, field.getName(), field.get(object)));
                }

            }
        }

        return result;
    }



    private static class DuplicateField {
        private String duplicateName;
        private String fieldName;
        private Object value;

        public DuplicateField(String duplicateName, String fieldName, Object value) {
            this.duplicateName = duplicateName;
            this.fieldName = fieldName;
            this.value = value;
        }

        public String getDuplicateName() {
            return duplicateName;
        }

        public String getFieldName() {
            return fieldName;
        }

        public Object getValue() {
            return value;
        }
    }
}
