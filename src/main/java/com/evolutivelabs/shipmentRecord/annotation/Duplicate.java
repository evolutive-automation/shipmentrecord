package com.evolutivelabs.shipmentRecord.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 用於複製欄位數值使用
 * @see com.evolutivelabs.shipmentRecord.utils.DuplicateUtils
 */
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface Duplicate {
    String value() default "";
    boolean customized() default false;
}
