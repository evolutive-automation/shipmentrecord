package com.evolutivelabs.shipmentRecord.model;

import com.evolutivelabs.shipmentRecord.annotation.Duplicate;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class KafkaMessagePayLoad implements Serializable {
    @Duplicate
    private Long id;

    @Duplicate("sourceId")
    private Long source_id;

    @Duplicate("sourceType")
    private String source_type;

    @Duplicate("customized")
    private Boolean is_customized;

    @Duplicate("shippingLabel")
    private String shipping_label;

    @Duplicate("trackingNumber")
    private String tracking_number;

    @Duplicate
    private String carrier;

    @Duplicate
    private String status;

    @Duplicate
    private String invoice;

    @Duplicate
    private String notes;

    @Duplicate
    private String tags;

    @Duplicate(value = "metaData", customized = true)
    private KafkaMessageMetaData meta_data;

    @Duplicate("estimatedDelivery")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime estimated_delivery;

    @Duplicate("shippingLabelUpdatedAt")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime shipping_label_updated_at;

    @Duplicate("processPriority")
    private String process_priority;

    @Duplicate("shipToCountry")
    private String ship_to_country;

    @Duplicate("sourceWebsite")
    private String source_website;

    @Duplicate
    private String allocation;

    @Duplicate("cancelledAt")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime cancelled_at; //TODO: time

    @Duplicate("pausedAt")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime paused_at;//TODO: time

    @Duplicate("createdAt")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime created_at;//TODO: time

    @Duplicate("updatedAt")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updated_at;//TODO: time

    private List<KafkaMessageItem> items;

    @Duplicate("isPaused")
    private Boolean is_paused;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSource_id() {
        return source_id;
    }

    public void setSource_id(Long source_id) {
        this.source_id = source_id;
    }

    public String getSource_type() {
        return source_type;
    }

    public void setSource_type(String source_type) {
        this.source_type = source_type;
    }

    public Boolean getIs_customized() {
        return is_customized;
    }

    public void setIs_customized(Boolean is_customized) {
        this.is_customized = is_customized;
    }

    public String getShipping_label() {
        return shipping_label;
    }

    public void setShipping_label(String shipping_label) {
        this.shipping_label = shipping_label;
    }

    public String getTracking_number() {
        return tracking_number;
    }

    public void setTracking_number(String tracking_number) {
        this.tracking_number = tracking_number;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public KafkaMessageMetaData getMeta_data() {
        return meta_data;
    }

    public void setMeta_data(KafkaMessageMetaData meta_data) {
        this.meta_data = meta_data;
    }

    public LocalDateTime getEstimated_delivery() {
        return estimated_delivery;
    }

    public void setEstimated_delivery(LocalDateTime estimated_delivery) {
        this.estimated_delivery = estimated_delivery;
    }

    public LocalDateTime getShipping_label_updated_at() {
        return shipping_label_updated_at;
    }

    public void setShipping_label_updated_at(LocalDateTime shipping_label_updated_at) {
        this.shipping_label_updated_at = shipping_label_updated_at;
    }

    public String getProcess_priority() {
        return process_priority;
    }

    public void setProcess_priority(String process_priority) {
        this.process_priority = process_priority;
    }

    public String getShip_to_country() {
        return ship_to_country;
    }

    public void setShip_to_country(String ship_to_country) {
        this.ship_to_country = ship_to_country;
    }

    public String getSource_website() {
        return source_website;
    }

    public void setSource_website(String source_website) {
        this.source_website = source_website;
    }

    public String getAllocation() {
        return allocation;
    }

    public void setAllocation(String allocation) {
        this.allocation = allocation;
    }

    public LocalDateTime getCancelled_at() {
        return cancelled_at;
    }

    public void setCancelled_at(LocalDateTime cancelled_at) {
        this.cancelled_at = cancelled_at;
    }

    public LocalDateTime getPaused_at() {
        return paused_at;
    }

    public void setPaused_at(LocalDateTime paused_at) {
        this.paused_at = paused_at;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }

    public LocalDateTime getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
    }

    public List<KafkaMessageItem> getItems() {
        return items;
    }

    public void setItems(List<KafkaMessageItem> items) {
        this.items = items;
    }

    public Boolean getIs_paused() {
        return is_paused;
    }

    public void setIs_paused(Boolean is_paused) {
        this.is_paused = is_paused;
    }

    @Override
    public String toString() {
        return "GcpMessagePayLoad{" +
                "id=" + id +
                ", source_id=" + source_id +
                ", source_type='" + source_type + '\'' +
                ", is_customized=" + is_customized +
                ", shipping_label='" + shipping_label + '\'' +
                ", tracking_number='" + tracking_number + '\'' +
                ", carrier='" + carrier + '\'' +
                ", status='" + status + '\'' +
                ", invoice='" + invoice + '\'' +
                ", notes='" + notes + '\'' +
                ", tags='" + tags + '\'' +
                ", meta_data=" + meta_data +
                ", estimated_delivery='" + estimated_delivery + '\'' +
                ", shipping_label_updated_at='" + shipping_label_updated_at + '\'' +
                ", process_priority='" + process_priority + '\'' +
                ", ship_to_country='" + ship_to_country + '\'' +
                ", source_website='" + source_website + '\'' +
                ", allocation='" + allocation + '\'' +
                ", cancelled_at='" + cancelled_at + '\'' +
                ", paused_at='" + paused_at + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", items=" + items +
                ", is_paused=" + is_paused +
                '}';
    }
}
