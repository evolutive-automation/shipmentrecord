package com.evolutivelabs.shipmentRecord.model;

import com.evolutivelabs.shipmentRecord.annotation.Duplicate;

import java.io.Serializable;

public class KafkaMessageMetaData implements Serializable {
    @Duplicate("doubleCheck")
    private String double_check;

    @Duplicate
    private String organization;

    @Duplicate("onlyPickingInventory")
    private Boolean only_picking_inventory;

    public String getDouble_check() {
        return double_check;
    }

    public void setDouble_check(String double_check) {
        this.double_check = double_check;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Boolean getOnly_picking_inventory() {
        return only_picking_inventory;
    }

    public void setOnly_picking_inventory(Boolean only_picking_inventory) {
        this.only_picking_inventory = only_picking_inventory;
    }

    @Override
    public String toString() {
        return "GcpMessageMetaData{" +
                "double_check='" + double_check + '\'' +
                ", organization='" + organization + '\'' +
                ", only_picking_inventory=" + only_picking_inventory +
                '}';
    }
}
