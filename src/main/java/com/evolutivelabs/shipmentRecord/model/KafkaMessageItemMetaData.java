package com.evolutivelabs.shipmentRecord.model;

import com.evolutivelabs.shipmentRecord.annotation.Duplicate;

import java.io.Serializable;

public class KafkaMessageItemMetaData implements Serializable {
    @Duplicate
    private String ccid;

    @Duplicate
    private String title;

    @Duplicate("bundleId")
    private String bundle_id;

    @Duplicate("isBundled")
    private Boolean is_bundled;

    @Duplicate("packageType")
    private String package_type;

    @Duplicate("hotstampFont")
    private String hotstamp_font;

    @Duplicate("hotstampText")
    private String hotstamp_text;

    @Duplicate("pasteSticker")
    private Boolean paste_sticker;

    @Duplicate("hotstampColor")
    private String hotstamp_color;

    @Duplicate("printImagePath")
    private String print_image_path;

    @Duplicate("previewImagePath")
    private String preview_image_path;

    public String getCcid() {
        return ccid;
    }

    public void setCcid(String ccid) {
        this.ccid = ccid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBundle_id() {
        return bundle_id;
    }

    public void setBundle_id(String bundle_id) {
        this.bundle_id = bundle_id;
    }

    public Boolean getIs_bundled() {
        return is_bundled;
    }

    public void setIs_bundled(Boolean is_bundled) {
        this.is_bundled = is_bundled;
    }

    public String getPackage_type() {
        return package_type;
    }

    public void setPackage_type(String package_type) {
        this.package_type = package_type;
    }

    public String getHotstamp_font() {
        return hotstamp_font;
    }

    public void setHotstamp_font(String hotstamp_font) {
        this.hotstamp_font = hotstamp_font;
    }

    public String getHotstamp_text() {
        return hotstamp_text;
    }

    public void setHotstamp_text(String hotstamp_text) {
        this.hotstamp_text = hotstamp_text;
    }

    public Boolean getPaste_sticker() {
        return paste_sticker;
    }

    public void setPaste_sticker(Boolean paste_sticker) {
        this.paste_sticker = paste_sticker;
    }

    public String getHotstamp_color() {
        return hotstamp_color;
    }

    public void setHotstamp_color(String hotstamp_color) {
        this.hotstamp_color = hotstamp_color;
    }

    public String getPrint_image_path() {
        return print_image_path;
    }

    public void setPrint_image_path(String print_image_path) {
        this.print_image_path = print_image_path;
    }

    public String getPreview_image_path() {
        return preview_image_path;
    }

    public void setPreview_image_path(String preview_image_path) {
        this.preview_image_path = preview_image_path;
    }

    @Override
    public String toString() {
        return "GcpMessageItemMetaData{" +
                "ccid='" + ccid + '\'' +
                ", title='" + title + '\'' +
                ", bundle_id='" + bundle_id + '\'' +
                ", is_bundled=" + is_bundled +
                ", package_type='" + package_type + '\'' +
                ", hotstamp_font='" + hotstamp_font + '\'' +
                ", hotstamp_text='" + hotstamp_text + '\'' +
                ", paste_sticker=" + paste_sticker +
                ", hotstamp_color='" + hotstamp_color + '\'' +
                ", print_image_path='" + print_image_path + '\'' +
                ", preview_image_path='" + preview_image_path + '\'' +
                '}';
    }
}
