package com.evolutivelabs.shipmentRecord.model;

import java.io.Serializable;

public class KafkaMessage implements Serializable {

    private String name;
    private KafkaMessagePayLoad payload;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KafkaMessagePayLoad getPayload() {
        return payload;
    }

    public void setPayload(KafkaMessagePayLoad payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "GcpMessage{" +
                "name='" + name + '\'' +
                ", payload=" + payload +
                '}';
    }
}
