package com.evolutivelabs.shipmentRecord.model;

import com.evolutivelabs.shipmentRecord.annotation.Duplicate;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

public class KafkaMessageItem implements Serializable {
    @Duplicate
    private Long id;

    @Duplicate("customized")
    private Short is_customized;

    @Duplicate("sourceItemId")
    private Long source_item_id;

    @Duplicate("orderNumber")
    private String order_number;

    @Duplicate
    private String sku;

    @Duplicate
    private Integer quantity;

    @Duplicate
    private String status;

    @Duplicate
    private Boolean ecn;

    @Duplicate(value = "metaData", customized = true)
    private KafkaMessageItemMetaData meta_data;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Duplicate("createdAt")
    private LocalDateTime created_at;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Duplicate("updatedAt")
    private LocalDateTime updated_at;

    @Duplicate("productTitle")
    private String product_title;

    @Duplicate("imagePath")
    private String image_path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getIs_customized() {
        return is_customized;
    }

    public void setIs_customized(Short is_customized) {
        this.is_customized = is_customized;
    }

    public Long getSource_item_id() {
        return source_item_id;
    }

    public void setSource_item_id(Long source_item_id) {
        this.source_item_id = source_item_id;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getEcn() {
        return ecn;
    }

    public void setEcn(Boolean ecn) {
        this.ecn = ecn;
    }

    public KafkaMessageItemMetaData getMeta_data() {
        return meta_data;
    }

    public void setMeta_data(KafkaMessageItemMetaData meta_data) {
        this.meta_data = meta_data;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }

    public LocalDateTime getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    @Override
    public String toString() {
        return "GcpMessageItem{" +
                "id=" + id +
                ", is_customized=" + is_customized +
                ", source_item_id=" + source_item_id +
                ", order_number='" + order_number + '\'' +
                ", sku='" + sku + '\'' +
                ", quantity=" + quantity +
                ", status='" + status + '\'' +
                ", ecn=" + ecn +
                ", meta_data=" + meta_data +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", product_title='" + product_title + '\'' +
                ", image_path='" + image_path + '\'' +
                '}';
    }
}
