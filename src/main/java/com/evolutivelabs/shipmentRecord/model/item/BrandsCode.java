package com.evolutivelabs.shipmentRecord.model.item;

public enum BrandsCode {
    APPLE("01"),
    GOOGLE("02"),
    SAMSUNG("03"),
    SONY("04"),
    ONEPLUS("09"),
    HUAWEI("10"),
    ASUS("11"),
    OPPO("13"),
    XIAOMI("14");

    private String code;

    BrandsCode(String code) {
        this.code = code;
    }

    public String code() {
        return code;
    }
}
