package com.evolutivelabs.shipmentRecord.model.item;

public class ItemCase {

    private CaseType caseType;
    private Integer quantity;
    private Integer androidQuantity;
    private Integer appleQuantity;

    public CaseType getCaseType() {
        return caseType;
    }

    public void setCaseType(CaseType caseType) {
        this.caseType = caseType;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getAndroidQuantity() {
        return androidQuantity;
    }

    public void setAndroidQuantity(Integer androidQuantity) {
        this.androidQuantity = androidQuantity;
    }

    public Integer getAppleQuantity() {
        return appleQuantity;
    }

    public void setAppleQuantity(Integer appleQuantity) {
        this.appleQuantity = appleQuantity;
    }
}
