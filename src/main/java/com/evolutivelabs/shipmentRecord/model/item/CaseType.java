package com.evolutivelabs.shipmentRecord.model.item;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum CaseType {
    ABA("airpod"),
    BT0("mod"),
    BTN("mod"),
    BTS("ssa"),
    CCA("cca"),
    DS0("mod"),
    DSA("airpod"),
    DSN("mod"),
    DSS("ssa"),
    FD0("mod"),
    FDA("airpod"),
    FDN("mod"),
    FDS("ssa"),
    HKS("ssa"),
    HP0("mod"),
    HPA("airpod"),
    HPN("mod"),
    HPS("ssa"),
    KI0("mod"),
    KPA("airpod"),
    KPB("mod"),
    KSS("ssa"),
    KX0("mod"),
    MKA("airpod"),
    MKN("mod"),
    MKS("ssa"),
    NB0("mod"),
    NBA("airpod"),
    NBN("mod"),
    NBS("ssa"),
    NPB("mod"),
    NX0("mod"),
    PM0("mod"),
    PMA("airpod"),
    PMN("mod"),
    PMS("ssa"),
    PX0("mod"),
    PXN("mod"),
    PXS("ssa"),
    SB0("mod"),
    SBN("mod"),
    SBS("ssa"),
    SSA("ssa"),
    SW0("mod"),
    SWN("mod");

    private String product;
    private static Map<String, CaseType> map = Collections.emptyMap();
    private static Map<String, List<CaseType>> productGroup = Collections.emptyMap();

    static {
        map = Arrays.stream(CaseType.values())
                .collect(Collectors.toUnmodifiableMap(CaseType::name, Function.identity()));

        productGroup = Arrays.stream(CaseType.values())
                .collect(Collectors.groupingBy(caseType -> caseType.getProduct()))
                .entrySet().stream()
                .map(entry -> new AbstractMap.SimpleEntry(entry.getKey(), Collections.unmodifiableList(entry.getValue())))
                .collect(Collectors.toUnmodifiableMap(Map.Entry<String, List<CaseType>>::getKey,
                        Map.Entry<String, List<CaseType>>::getValue));
    }

    CaseType(String product) {
        this.product = product;
    }

    public String getProduct() {
        return product;
    }

    public static List<CaseType> getCaseTypesByProduct(String product) {
        return productGroup.containsKey(product) ? productGroup.get(product) : Collections.emptyList();
    }
}
