package com.evolutivelabs.shipmentRecord.config;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.*;
import org.springframework.kafka.listener.BatchErrorHandler;
import org.springframework.kafka.listener.ContainerProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * kafka設定檔
 */
@Configuration
public class KafkaConfig {
    private static final Logger logger = LoggerFactory.getLogger(KafkaConfig.class);

    private final KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

    private final EvolutivelabsProperties evolutivelabsProperties;

    public KafkaConfig(KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry, EvolutivelabsProperties evolutivelabsProperties) {
        this.kafkaListenerEndpointRegistry = kafkaListenerEndpointRegistry;
        this.evolutivelabsProperties = evolutivelabsProperties;
    }

    /**
     * 建立預設的錯誤topic
     *
     * @return
     */
    @Bean
    public NewTopic errorTopic() {
        return TopicBuilder.name(evolutivelabsProperties.getErrorTopicShipment())
                .partitions(evolutivelabsProperties.getPartitions())
                .replicas(evolutivelabsProperties.getReplicas())
                .build();
    }

    /**
     * 建立producer相關設定
     *
     * @param consumerFactory
     * @return
     */
    @Bean
    public ProducerFactory producerFactory(ConsumerFactory<String, String> consumerFactory) {
        Map<String, Object> configs = new HashMap<>();
        Map<String, Object> consumerConfigs = consumerFactory.getConfigurationProperties();
        configs.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, consumerConfigs.get(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG));
        configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new DefaultKafkaProducerFactory(configs);
    }

    @Bean
    public KafkaTemplate kafkaTemplate(ProducerFactory producerFactory) {
        return new KafkaTemplate(producerFactory);
    }

    @Bean
    public KafkaAdmin kafkaAdmin(ConsumerFactory<String, String> consumerFactory) {
        Map<String, Object> properties = new HashMap<>();
        properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, consumerFactory.getConfigurationProperties().get(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG));
        return new KafkaAdmin(properties);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String>
    kafkaListenerContainerFactory(ConsumerFactory<String, String> consumerFactory, KafkaTemplate kafkaTemplate) {
        ConcurrentKafkaListenerContainerFactory<String, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConcurrency(evolutivelabsProperties.getConcurrency());
        factory.setConsumerFactory(consumerFactory);
        factory.setBatchListener(true);
        factory.getContainerProperties().setIdleBetweenPolls(evolutivelabsProperties.getFetchTime()); // listener 間隔時間
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.MANUAL_IMMEDIATE);

        factory.setBatchErrorHandler(new BatchErrorHandler() {

            @Override
            public void handle(Exception thrownException, ConsumerRecords<?, ?> datas) {
                logger.error(thrownException.getMessage(), thrownException);
                datas.forEach(data ->
                        kafkaTemplate.send(evolutivelabsProperties.getErrorTopicShipment(), data.value()));
            }
        });
        return factory;
    }

}
