package com.evolutivelabs.shipmentRecord.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 相關設定共用的properties
 */
@Component
public class EvolutivelabsProperties {
    @Value("${evolutivelabs.kafka.input.topic.shipment.name}")
    private String topicShipment;

    @Value("${evolutivelabs.kafka.input.listen.id.shipment}")
    private String listenIdShipment;

    @Value("${evolutivelabs.kafka.input.topic.shipment.partitions}")
    private Integer partitions;

    @Value("${evolutivelabs.kafka.input.topic.shipment.replicas}")
    private Integer replicas;

    @Value("${evolutivelabs.kafka.consumer.fetch.time}")
    private Long fetchTime;

    @Value("${evolitibelabs.kafka.input.listen.concurrency}")
    private Integer concurrency;

    public String getTopicShipment() {
        return topicShipment;
    }

    public String getListenIdShipment() {
        return listenIdShipment;
    }

    public String getErrorTopicShipment() {
        return "error.".concat(topicShipment);
    }

    public Integer getPartitions() {
        return partitions;
    }

    public Integer getReplicas() {
        return replicas;
    }

    public Long getFetchTime() {
        return fetchTime;
    }

    public Integer getConcurrency() {
        return concurrency;
    }
}
