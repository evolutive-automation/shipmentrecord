package com.evolutivelabs.shipmentRecord.database.entity;

import com.evolutivelabs.shipmentRecord.annotation.Duplicate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
public class Shipment extends BaseEntity {

    /**
     * 1
     * 主鍵
     */
    @Id
    @Column(unique = true, nullable = false)
    @Duplicate
    private Long id;

    /**
     * 2
     * 裝態
     */
    @Column(nullable = false)
    @Duplicate
    private String status;

    /**
     * 3
     * TODO: 取值?, nullable = false
     */
    @Column
    private String station;

    /**
     * 4
     * 是否為客製品
     */
    @Column(name = "is_customized", nullable = false)
    @Duplicate
    private Boolean customized;

    /**
     * 5
     * 是否取消
     * TODO: 如何取值?
     */
    @Column(name = "is_cancelled", nullable = false)
    private Boolean cancelled = false;

    /**
     * 6
     * 是否暫停
     * TODO: 如何取值
     */
    @Column(name = "is_paused", nullable = false)
    @Duplicate("isPaused")
    private Boolean paused = false;

    /**
     * 7
     */
    @Column(name = "source_id", nullable = false)
    @Duplicate
    private Long sourceId;

    /**
     * 8
     */
    @Column(name = "source_type", nullable = false)
    @Duplicate
    private String sourceType;

    /**
     * 9
     */
    @Column(nullable = false)
    @Duplicate
    private String carrier;

    /**
     * 10
     */
    @Column(columnDefinition = "text")
    @Duplicate
    private String notes;

    /**
     * 11
     */
    @Column(nullable = false)
    @Duplicate
    private String tags;

    /**
     * 12
     */
    @Column(name = "shipping_label")
    @Duplicate
    private String shippingLabel;

    /**
     * 13
     */
    @Column(name = "shipping_label_updated_at", columnDefinition = "timestamp")
    @Duplicate
    private LocalDateTime shippingLabelUpdatedAt;

    /**
     * 14
     * TODO: nullable = false
     */
    @Column(name = "shipping_label_path")
    private String shippingLabelPath;

    /**
     * 15
     */
    @Column(name = "process_priority", nullable = false)
    @Duplicate
    private String processPriority;

    /**
     * 16
     */
    @Column(name = "ship_to_country", nullable = false)
    @Duplicate
    private String shipToCountry;

    /**
     * 17
     */
    @Column(name = "source_website", nullable = false)
    @Duplicate
    private String sourceWebsite;

    /**
     * 18
     */
    @Column
    @Duplicate
    private String allocation;

    /**
     * 19
     */
    @Column(name = "tracking_number", nullable = false)
    @Duplicate
    private String trackingNumber;

    /**
     * 20
     */
    @Column(columnDefinition = "text")
    @Duplicate
    private String invoice;

    /**
     * 21. 取消時間
     */
    @Column(name = "cancelled_at", columnDefinition = "timestamp")
    @Duplicate
    private LocalDateTime cancelledAt;

    /**
     * 22. 暫停時間
     */
    @Column(name = "paused_at", columnDefinition = "timestamp")
    @Duplicate
    private LocalDateTime pausedAt;

    /**
     * 23
     */
    @Column(name = "estimated_delivery", columnDefinition = "timestamp")
    @Duplicate
    private LocalDateTime estimatedDelivery;

    /**
     * 24
     * TODO: nullable = false
     */
    @Column(name = "webhook_updated_at", columnDefinition = "timestamp")
    private LocalDateTime webhookUpdatedAt;

    /**
     * 25
     * TODO: nullable = false
     */
    @Column(name = "webhook_created_at", columnDefinition = "timestamp")
    private LocalDateTime webhookCreatedAt;

    /**
     * 26
     */
    @Column(name = "created_at", columnDefinition = "timestamp")
    @Duplicate
    private LocalDateTime createdAt;

    /**
     * 27
     */
    @Column(name = "updated_at", columnDefinition = "timestamp")
    @Duplicate
    private LocalDateTime updatedAt;

    /**
     * 28
     * TODO:
     */
    @Column(name = "shipping_label_printed_at", columnDefinition = "timestamp")
    private LocalDateTime shippingLabelPrintedAt;

    /**
     * 29
     */
    @Type(type = "json")
    @Column(name = "meta_data", columnDefinition = "json")
    @Duplicate(customized = true)
    private ShipmentMetaData metaData;

    /**
     * 30
     * TODO:
     */
    @Column(name = "process_type")
    private String processType;

    /**
     * 31
     */
    @Column(name = "receive_time", nullable = false, columnDefinition = "timestamp")
    private LocalDateTime receiveTime;

    /**
     * 32
     */
    @Column(length = 10)
    private String type;

    @Column(name = "ssa_android_quantity")
    private Integer ssaAndroidQuantity;

    @Column(name = "ssa_apple_quantity")
    private Integer ssaAppleQuantity;

    @Column(name = "mod_quantity")
    private Integer modQuantity;

    @Column(name = "cca_quantity")
    private Integer ccaQuantity;

    @Column(name = "airpod_quantity")
    private Integer airpodQuantity;

    /**
     * 二進制分類，是否有以下產品
     * SSA MOD CCA AIRPOD
     * 1000 = 8
     * 1100 = 12
     */
    @Column
    private Integer classification;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public Boolean getCustomized() {
        return customized;
    }

    public void setCustomized(Boolean customized) {
        this.customized = customized;
    }

    public Boolean getCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Boolean getPaused() {
        return paused;
    }

    public void setPaused(Boolean paused) {
        this.paused = paused;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getShippingLabel() {
        return shippingLabel;
    }

    public void setShippingLabel(String shippingLabel) {
        this.shippingLabel = shippingLabel;
    }

    public LocalDateTime getShippingLabelUpdatedAt() {
        return shippingLabelUpdatedAt;
    }

    public void setShippingLabelUpdatedAt(LocalDateTime shippingLabelUpdatedAt) {
        this.shippingLabelUpdatedAt = shippingLabelUpdatedAt;
    }

    public String getShippingLabelPath() {
        return shippingLabelPath;
    }

    public void setShippingLabelPath(String shippingLabelPath) {
        this.shippingLabelPath = shippingLabelPath;
    }

    public String getProcessPriority() {
        return processPriority;
    }

    public void setProcessPriority(String processPriority) {
        this.processPriority = processPriority;
    }

    public String getShipToCountry() {
        return shipToCountry;
    }

    public void setShipToCountry(String shipToCountry) {
        this.shipToCountry = shipToCountry;
    }

    public String getSourceWebsite() {
        return sourceWebsite;
    }

    public void setSourceWebsite(String sourceWebsite) {
        this.sourceWebsite = sourceWebsite;
    }

    public String getAllocation() {
        return allocation;
    }

    public void setAllocation(String allocation) {
        this.allocation = allocation;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public LocalDateTime getCancelledAt() {
        return cancelledAt;
    }

    public void setCancelledAt(LocalDateTime cancelledAt) {
        this.cancelledAt = cancelledAt;
    }

    public LocalDateTime getPausedAt() {
        return pausedAt;
    }

    public void setPausedAt(LocalDateTime pausedAt) {
        this.pausedAt = pausedAt;
    }

    public LocalDateTime getEstimatedDelivery() {
        return estimatedDelivery;
    }

    public void setEstimatedDelivery(LocalDateTime estimatedDelivery) {
        this.estimatedDelivery = estimatedDelivery;
    }

    public LocalDateTime getWebhookUpdatedAt() {
        return webhookUpdatedAt;
    }

    public void setWebhookUpdatedAt(LocalDateTime webhookUpdatedAt) {
        this.webhookUpdatedAt = webhookUpdatedAt;
    }

    public LocalDateTime getWebhookCreatedAt() {
        return webhookCreatedAt;
    }

    public void setWebhookCreatedAt(LocalDateTime webhookCreatedAt) {
        this.webhookCreatedAt = webhookCreatedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getShippingLabelPrintedAt() {
        return shippingLabelPrintedAt;
    }

    public void setShippingLabelPrintedAt(LocalDateTime shippingLabelPrintedAt) {
        this.shippingLabelPrintedAt = shippingLabelPrintedAt;
    }

    public ShipmentMetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(ShipmentMetaData metaData) {
        this.metaData = metaData;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public LocalDateTime getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(LocalDateTime receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSsaAndroidQuantity() {
        return ssaAndroidQuantity;
    }

    public void setSsaAndroidQuantity(Integer ssaAndroidQuantity) {
        this.ssaAndroidQuantity = ssaAndroidQuantity;
    }

    public Integer getSsaAppleQuantity() {
        return ssaAppleQuantity;
    }

    public void setSsaAppleQuantity(Integer ssaAppleQuantity) {
        this.ssaAppleQuantity = ssaAppleQuantity;
    }

    public Integer getModQuantity() {
        return modQuantity;
    }

    public void setModQuantity(Integer modQuantity) {
        this.modQuantity = modQuantity;
    }

    public Integer getCcaQuantity() {
        return ccaQuantity;
    }

    public void setCcaQuantity(Integer ccaQuantity) {
        this.ccaQuantity = ccaQuantity;
    }

    public Integer getAirpodQuantity() {
        return airpodQuantity;
    }

    public void setAirpodQuantity(Integer airpodQuantity) {
        this.airpodQuantity = airpodQuantity;
    }

    public Integer getClassification() {
        return classification;
    }

    public void setClassification(Integer classification) {
        this.classification = classification;
    }

    @Override
    public String toString() {
        return "Shipment{" +
                "id=" + id +
                ", status='" + status + '\'' +
                ", station='" + station + '\'' +
                ", customized=" + customized +
                ", cancelled=" + cancelled +
                ", paused=" + paused +
                ", sourceId=" + sourceId +
                ", sourceType='" + sourceType + '\'' +
                ", carrier='" + carrier + '\'' +
                ", notes='" + notes + '\'' +
                ", tags='" + tags + '\'' +
                ", shippingLabel='" + shippingLabel + '\'' +
                ", shippingLabelUpdatedAt=" + shippingLabelUpdatedAt +
                ", shippingLabelPath='" + shippingLabelPath + '\'' +
                ", processPriority='" + processPriority + '\'' +
                ", shipToCountry='" + shipToCountry + '\'' +
                ", sourceWebsite='" + sourceWebsite + '\'' +
                ", allocation='" + allocation + '\'' +
                ", trackingNumber='" + trackingNumber + '\'' +
                ", invoice='" + invoice + '\'' +
                ", cancelledAt=" + cancelledAt +
                ", pausedAt=" + pausedAt +
                ", estimatedDelivery=" + estimatedDelivery +
                ", webhookUpdatedAt=" + webhookUpdatedAt +
                ", webhookCreatedAt=" + webhookCreatedAt +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", shippingLabelPrintedAt=" + shippingLabelPrintedAt +
                ", metaData=" + metaData +
                ", processType='" + processType + '\'' +
                ", receiveTime=" + receiveTime +
                ", type='" + type + '\'' +
                ", ssaAndroidQuantity=" + ssaAndroidQuantity +
                ", ssaAppleQuantity=" + ssaAppleQuantity +
                ", modQuantity=" + modQuantity +
                ", ccaQuantity=" + ccaQuantity +
                ", airpodQuantity=" + airpodQuantity +
                ", classification=" + classification +
                '}';
    }
}
