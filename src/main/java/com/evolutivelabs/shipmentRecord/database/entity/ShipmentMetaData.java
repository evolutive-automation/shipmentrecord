package com.evolutivelabs.shipmentRecord.database.entity;

import com.evolutivelabs.shipmentRecord.annotation.Duplicate;

import java.io.Serializable;

public class ShipmentMetaData implements Serializable {
    @Duplicate
    private String doubleCheck;

    @Duplicate
    private String organization;

    @Duplicate
    private Boolean onlyPickingInventory;

    public String getDoubleCheck() {
        return doubleCheck;
    }

    public void setDoubleCheck(String doubleCheck) {
        this.doubleCheck = doubleCheck;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Boolean getOnlyPickingInventory() {
        return onlyPickingInventory;
    }

    public void setOnlyPickingInventory(Boolean onlyPickingInventory) {
        this.onlyPickingInventory = onlyPickingInventory;
    }

    @Override
    public String toString() {
        return "ShipmentMetaData{" +
                "doubleCheck='" + doubleCheck + '\'' +
                ", organization='" + organization + '\'' +
                ", onlyPickingInventory=" + onlyPickingInventory +
                '}';
    }
}
