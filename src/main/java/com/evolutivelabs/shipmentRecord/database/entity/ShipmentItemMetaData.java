package com.evolutivelabs.shipmentRecord.database.entity;

import com.evolutivelabs.shipmentRecord.annotation.Duplicate;

import java.io.Serializable;

public class ShipmentItemMetaData implements Serializable {
    @Duplicate
    private String title;

    @Duplicate
    private String ccid;

    @Duplicate
    private String printImagePath;

    @Duplicate
    private String previewImagePath;

    @Duplicate
    private Boolean pasteSticker;

    @Duplicate
    private String hotstampText;

    @Duplicate
    private String hotstampFont;

    @Duplicate
    private String hotstampColor;

    @Duplicate
    private String bundleId;

    @Duplicate
    private Boolean isBundled;

    @Duplicate
    private String packageType;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCcid() {
        return ccid;
    }

    public void setCcid(String ccid) {
        this.ccid = ccid;
    }

    public String getPrintImagePath() {
        return printImagePath;
    }

    public void setPrintImagePath(String printImagePath) {
        this.printImagePath = printImagePath;
    }

    public String getPreviewImagePath() {
        return previewImagePath;
    }

    public void setPreviewImagePath(String previewImagePath) {
        this.previewImagePath = previewImagePath;
    }

    public Boolean getPasteSticker() {
        return pasteSticker;
    }

    public void setPasteSticker(Boolean pasteSticker) {
        this.pasteSticker = pasteSticker;
    }

    public String getHotstampText() {
        return hotstampText;
    }

    public void setHotstampText(String hotstampText) {
        this.hotstampText = hotstampText;
    }

    public String getHotstampFont() {
        return hotstampFont;
    }

    public void setHotstampFont(String hotstampFont) {
        this.hotstampFont = hotstampFont;
    }

    public String getHotstampColor() {
        return hotstampColor;
    }

    public void setHotstampColor(String hotstampColor) {
        this.hotstampColor = hotstampColor;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public Boolean getBundled() {
        return isBundled;
    }

    public void setBundled(Boolean bundled) {
        isBundled = bundled;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    @Override
    public String toString() {
        return "ShipmentItemMetaData{" +
                "title='" + title + '\'' +
                ", ccid='" + ccid + '\'' +
                ", printImagePath='" + printImagePath + '\'' +
                ", previewImagePath='" + previewImagePath + '\'' +
                ", pasteSticker=" + pasteSticker +
                ", hotstampText='" + hotstampText + '\'' +
                ", hotstampFont='" + hotstampFont + '\'' +
                ", hotstampColor='" + hotstampColor + '\'' +
                ", bundleId='" + bundleId + '\'' +
                ", isBundled=" + isBundled +
                ", packageType='" + packageType + '\'' +
                '}';
    }
}
