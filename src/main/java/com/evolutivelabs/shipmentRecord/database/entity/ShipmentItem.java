package com.evolutivelabs.shipmentRecord.database.entity;

import com.evolutivelabs.shipmentRecord.annotation.Duplicate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "shipment_item")
public class ShipmentItem extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    @Duplicate
    private Long id;

    @Column(name = "shipment_id", nullable = false)
    private Long shipmentId;

    @Column(name = "source_item_id", nullable = false)
    @Duplicate
    private Long sourceItemId;

    @Column(name = "is_customized", nullable = false)
    @Duplicate
    private Short customized;

    // TODO: 取值?
    @Column(name = "bundle_id")
    private String bundleId;

    @Column(nullable = false)
    @Duplicate
    private String status;

    @Column(nullable = false)
    @Duplicate
    private Boolean ecn;

    @Column(nullable = false)
    @Duplicate
    private String sku;

    @Column(name = "product_title", nullable = false)
    @Duplicate
    private String productTitle;

    // TODO: productTitle ???
    @Column(name = "display_title", nullable = false)
    private String displayTitle;

    @Column(nullable = false)
    @Duplicate
    private Integer quantity;

    @Column(name = "image_path", columnDefinition = "longtext")
    @Duplicate
    private String imagePath;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    @Duplicate(customized = true)
    private ShipmentItemMetaData metaData;

    /**
     * TODO: 怎麼轉換??, nullable = false
     */
    @Column(length = 30)
    private String checkSku;

    /**
     * TODO: 取值?, nullable = false
     */
    @Column(length = 200)
    private String locations;

    @Column(name = "order_number", nullable = false)
    @Duplicate
    private String orderNumber;

    /**
     * TODO: 取值?, nullable = false
     */
    @Column(name = "shipment_items_count")
    private Integer shipmentItemsCount;

    /**
     * TODO: 取值?, nullable = false
     */
    @Column
    private Integer sequence;

    @Column(name = "created_at", columnDefinition = "timestamp")
    @Duplicate
    private LocalDateTime createdAt;

    @Column(name = "updated_at", columnDefinition = "timestamp")
    private LocalDateTime updatedAt;

    @Column(name = "case_sku", length = 30)
    private String caseSku;

    @Column(name = "case_type_code", length = 3)
    private String caseTypeCode;

    @Column(name = "brand_code", length = 2)
    private String brandCode;

    @Column(name = "color_code", length = 2)
    private String colorCode;

    @Column(name = "device_code", length = 3)
    private String deviceCode;

    @Column(name = "design_code", length = 10)
    private String designCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(Long shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Long getSourceItemId() {
        return sourceItemId;
    }

    public void setSourceItemId(Long sourceItemId) {
        this.sourceItemId = sourceItemId;
    }

    public Short getCustomized() {
        return customized;
    }

    public void setCustomized(Short customized) {
        this.customized = customized;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getEcn() {
        return ecn;
    }

    public void setEcn(Boolean ecn) {
        this.ecn = ecn;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public ShipmentItemMetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(ShipmentItemMetaData metaData) {
        this.metaData = metaData;
    }

    public String getCheckSku() {
        return checkSku;
    }

    public void setCheckSku(String checkSku) {
        this.checkSku = checkSku;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getShipmentItemsCount() {
        return shipmentItemsCount;
    }

    public void setShipmentItemsCount(Integer shipmentItemsCount) {
        this.shipmentItemsCount = shipmentItemsCount;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCaseSku() {
        return caseSku;
    }

    public void setCaseSku(String caseSku) {
        this.caseSku = caseSku;
    }

    public String getCaseTypeCode() {
        return caseTypeCode;
    }

    public void setCaseTypeCode(String caseTypeCode) {
        this.caseTypeCode = caseTypeCode;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getDesignCode() {
        return designCode;
    }

    public void setDesignCode(String designCode) {
        this.designCode = designCode;
    }

    @Override
    public String toString() {
        return "ShipmentItem{" +
                "id=" + id +
                ", shipmentId=" + shipmentId +
                ", sourceItemId=" + sourceItemId +
                ", customized=" + customized +
                ", bundleId='" + bundleId + '\'' +
                ", status='" + status + '\'' +
                ", ecn=" + ecn +
                ", sku='" + sku + '\'' +
                ", productTitle='" + productTitle + '\'' +
                ", displayTitle='" + displayTitle + '\'' +
                ", quantity=" + quantity +
                ", imagePath='" + imagePath + '\'' +
                ", metaData=" + metaData +
                ", checkSku='" + checkSku + '\'' +
                ", locations='" + locations + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                ", shipmentItemsCount=" + shipmentItemsCount +
                ", sequence=" + sequence +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", caseSku='" + caseSku + '\'' +
                ", caseTypeCode='" + caseTypeCode + '\'' +
                ", brandCode='" + brandCode + '\'' +
                ", colorCode='" + colorCode + '\'' +
                ", deviceCode='" + deviceCode + '\'' +
                ", designCode='" + designCode + '\'' +
                '}';
    }
}
