package com.evolutivelabs.shipmentRecord.database.repository;

import com.evolutivelabs.shipmentRecord.database.entity.ShipmentItem;
import org.springframework.data.repository.CrudRepository;

public interface ShipmentItemRepository extends CrudRepository<ShipmentItem, Long> {
}
