package com.evolutivelabs.shipmentRecord.database.repository;

import com.evolutivelabs.shipmentRecord.database.entity.Shipment;
import org.springframework.data.repository.CrudRepository;

public interface ShipmentRepository extends CrudRepository<Shipment, Long> {
}
