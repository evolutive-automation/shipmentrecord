package com.evolutivelabs.shipmentRecord.processor;

import com.evolutivelabs.shipmentRecord.config.EvolutivelabsProperties;
import com.evolutivelabs.shipmentRecord.model.KafkaMessage;
import com.evolutivelabs.shipmentRecord.service.ShipmentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * kafka處理器，將kafka資料轉換成shipment table
 */
@Component
public class KafkaProcessor {
    private static final Logger logger = LoggerFactory.getLogger(KafkaProcessor.class);

    private final ObjectMapper objectMapper;
    private final ShipmentService shipmentService;

    public KafkaProcessor(ObjectMapper objectMapper,
                          ShipmentService shipmentService) {
        this.objectMapper = objectMapper;
        this.shipmentService = shipmentService;
    }

    /**
     * 監聽kafka
     * @param records
     * @param acknowledgment
     * @throws Exception
     */
    @KafkaListener(id = "#{evolutivelabsProperties.listenIdShipment}", topics = "#{evolutivelabsProperties.topicShipment}")
    public void listen(List<ConsumerRecord<String, String>> records, Acknowledgment acknowledgment) throws Exception {
        LocalDateTime now = LocalDateTime.now();
        List<KafkaMessage> messages = new ArrayList<>();
        for (ConsumerRecord<String, String> record : records) {
            messages.add(objectMapper.readValue(record.value(), KafkaMessage.class));
        }

        logger.info("kafka pull message size: {}", messages.size());

        List<KafkaMessage> newMessages = messages.stream()
                .sorted((v1, v2) -> v1.getPayload().getUpdated_at().compareTo(v2.getPayload().getUpdated_at()))
                .collect(Collectors.toList());

        shipmentService.saveMessage(newMessages, now);

        if (logger.isDebugEnabled())
            logger.debug("begin: {}, end: {}", now, LocalDateTime.now());

        acknowledgment.acknowledge();

    }

}
