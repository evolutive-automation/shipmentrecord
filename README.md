# 啟動方式
* 打包
```shell
mvn clean install
```
* 啟動
```shell
cd ./target
# 預設spring.profiles.active=dev，啟動後為開發環境
java -jar shipmentRecord.jar
# 可設定啟動後為測試環境
java -jar -Dspring.profiles.active=test shipmentRecord.jar
```

---

# 環境變數設定
## Spring 設定
| 參數 | 預設值 | 名稱 |
|:---:|:-----:|:----:|
| SERVER_PORT | 8080 | 啟動時佔用的port |
| SPRING_PROFILES_ACTIVE | dev | 啟動要用的環境變數，判斷是正式或開發或測試環境 |

## kafka 連線設定
| 參數 | 預設值 | 名稱 |
|:---:|:-----:|:----:| 
| SPRING_KAFKA_CONSUMER_BOOTSTRAPSERVERS | localhost:9092 | ip |
| SPRING_KAFKA_CONSUMER_MAXPOLLRECORDS | 1000 | 每次抓取上限筆數 |
| SPRING_KAFKA_CONSUMER_GROUPID | shipment.created.pipe.group | group id |

## kafka 其餘參數
| 參數 | 預設值 | 名稱 |
|:---:|:-----:|:----:|
| SPRING_KAFKA_CONSUMER_MAXPOLLINTERVALMS | 10000 | 每次抓取時間上限 |
| SPRING_KAFKA_CONSUMER_PROPERTIES_RECONNECT_BACKOFF_MS | 10000 | 重新連線時間 |
| SPRING_KAFKA_CONSUMER_PROPERTIES_RECONNECT_BACKOFF_MAX_MS | 60000 | 重新連線時間上限 |

## 資料庫連線
| 參數 | 預設值 | 名稱 |
|:---:|:-----:|:----:|
| SPRING_DATASOURCE_URL | jdbc:mysql://10.60.1.220:3306/nxl_data?rewriteBatchedStatements=true&serverTimezone=Asia/Taipei&characterEncoding=utf-8 | 資料庫位置 |
| SPRING_DATASOURCE_USERNAME | nxl-user | 使用者 |
| SPRING_DATASOURCE_PASSWORD | rhinO5h!eld | 密碼 |

## 其餘設定
| 參數 | 預設值 | 名稱 |
|:---:|:-----:|:----:|
| EVOLUTIVELABS_KAFKA_INPUT_TOPIC_SHIPMENT_NAME | shipment.created.pipe | 抓取的topic名稱 |
| EVOLUTIVELABS_KAFKA_INPUT_LISTEN_ID_SHIPMENT | shipment.created | listen id (group id) |
| EVOLUTIVELABS_KAFKA_INPUT_TOPIC_SHIPMENT_PARTITIONS | 3 | 分區數 |
| EVOLUTIVELABS_KAFKA_INPUT_TOPIC_SHIPMENT_REPLICAS | 3 | 備份數 |
| EVOLUTIVELABS_KAFKA_INPUT_LISTEN_CONCURRENCY | 3 | 同時並行數量 |
| EVOLUTIVELABS_KAFKA_CONSUMER_FETCH_TIME | 3000 | 預計時間抓取資料 |
| EVOLUTIVELABS_LOGGING_PATH | /home/user/logs/shipmentRecord | log放置目錄 |